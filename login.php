<?php require_once("functions.inc");
$user=new User;
$user->logout(); ?>

<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="js/login.js"></script>
	<link rel="stylesheet" type="text/css" href="css/form.css">
</head>
<body>
	<form id="loginForm" method="POST" action="login-process.php">
		<div>
			<fieldset>
				<legend>Login</legend>
				<div id="errorDiv">
					<?php 
						if(isset($_SESSION['error']) && isset($_SESSION['formAttempt']))
						{
							unset($_SESSION['formAttempt']);
							print "Errors encountered <br/> \n";
							foreach($_SESSION['error'] as $error) { print $error. "<br/>\n";}
						}
					?>
				</div>
				<label for="uname">Username:*</label>
				<input type="text" id="uname" name="uname">
				<span class="errorFeedback errorSpan" id="unameError">Username required</span><br/>
				<label for="password">Password:*</label>
				<input type="password" id="password" name="password">
				<span class="errorFeedback errorSpan" id="passwordError">Password required</span><br/>
				<input type="submit" name="submit" id="submit"><br/>
			</fieldset>
		</div>
	</form>
	<div>
		<a href="home.php">Back to home page</a>
	</div>
</body>
</html>