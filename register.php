<?php require_once("functions.inc"); ?>

<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript" src="js/register.js"></script>
	<link rel="stylesheet" type="text/css" href="css/form.css">
	<title>A form</title>
</head>
<body>
	<form id="userForm" method="POST" action="register-process.php">
		<div>
			<fieldset>
				<legend>Registration Information</legend>
				<div id="errorDiv">
					<?php
						if(isset($_SESSION['error']) && isset($_SESSION['formAttempt']))
						{
							unset($_SESSION['formAttempt']);
							print "Errors ecountered <br/> \n";
							foreach($_SESSION['error'] as $error) {print $error. "<br/>\n";}	
						}
					?>
				</div>
				<label for="uname">Username:*</label>
				<input type="text" name="uname" id="uname">
				<span class="errorFeedback errorSpan" id="unameError">Username is required</span><br/>
				<label for="password1">Password:*</label>
				<input type="password" name="password1" id="password1">
				<span class="errorFeedback errorSpan" id="password1Error">Password is required</span><br/>
				<label for="password2">Verify Password:*</label>
				<input type="password" name="password2" id="password2">
				<span class="errorFeedback errorSpan" id="password2Error">Passwords don't match</span><br/>
				<input type="submit" name="submit" id="submit">	
			</fieldset>
		</div>
	</form>	
</body>
</html>
