<?php require_once("functions.inc");?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1">
	<title>WIZIM MOVIES</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/home.css">		
</head>
<body>
<header>
	<div class="container">
	    <div class="header-right">
	    	<a href="addmovie.php"><span>Add Movie</span></a>
		  	<a href="login.php"><span id="login">Login</span></a>
		  	<a href="register.php"><span>Register</span></a>
  		</div>
  	</div>
</header>
<div class="container">
	<div id="big-title">Movies</div>
		<?php 
		$mysqli=new mysqli(DBHOST,DBUSER,DBPASS,DB);
		if ($mysqli->connect_errno) 
		{
			error_log("Can't connect to MYSQL" .$mysqli->connect_errno);
			return false;
		}
  		$query="SELECT *,IFNULL((select SUM(rating)/count(rating) from ratings where title=m.title),0) as avgRating from Movies as m";
  		$result=$mysqli->query($query);
  		while($row = $result->fetch_assoc()){
  		?>
  		<div class="row movie-entry">
  			<div class="col-lg-12">
  				<div class="movie-title"><?php echo $row["title"];?></div>
  				<div class="genre"><?php echo $row["genre"];?></div>
  				<div class="w-100"></div>
				<img src="uploads/<?php echo $row["image"];?>">
				<div class="description"><?php echo $row["description"];?></div>
				<div class="rating"><?php echo $row["avgRating"];?></div>
			</div>
		</div>	
		<?php } ?>

	</div>
</body>
</html>