<?php
require_once("functions.inc");

$user=new User;

if(!$user->isLoggedIn){ die(header("Location: login.php"));}
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/home.css">
	<link rel="stylesheet" type="text/css" href="css/profile.css">
	<title><?php echo "{$user->username}"."'s profile"?></title>
</head>
<body>
	<header>
	<div class="container">
		<?php print "<span> Welcome {$user->username}</span>";?>
	    <div class="header-right">
		  	<a href="logout.php"><span>Logout</span></a>
  		</div>
  	</div>
</header>
<div class="container">
	<div id="big-title">Movies</div>
		<?php 
		$mysqli=new mysqli(DBHOST,DBUSER,DBPASS,DB);
		if ($mysqli->connect_errno) 
		{
			error_log("Can't connect to MYSQL" .$mysqli->connect_errno);
			return false;
		}
  		$query="SELECT * from Movies";
  		$result=$mysqli->query($query);
  		while($row = $result->fetch_assoc()){
  		?>
  		<div class="row movie-entry">
  			<div class="col-lg-12">
  				<div class="movie-title"><?php echo $row["title"];?></div>
  				<div class="genre"><?php echo $row["genre"];?></div>
  				<div class="w-100"></div>
				<img src="uploads/<?php echo $row["image"];?>">
				<div class="description"><?php echo $row["description"];?></div>
				<section class='rating-widget'>
				 <div class='rating-stars'>
				    <ul class='stars'>
				      <li class='star' data-value='1'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' data-value='2'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' data-value='3'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' data-value='4'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' data-value='5'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				    </ul>
				  </div>
				  <div class="rating"></div>
				  <form action="rating.php" method="POST" class="rating-form">
				  	<input type="hidden" name="movie" value="<?php echo $row["title"];?>" />
				  	<input type="hidden" name="rating" />
					<input type="submit" value="submit" />				  	
				  </form>
				</section>
			</div>
		</div>	
		<?php } ?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js/profile.js"></script>

</body>
</html>