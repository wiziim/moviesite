$(document).ready(function(){

  $('.stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10);

    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  $('.stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10);
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

  $ratingValue = parseInt($(this).parents(".stars").first().find("li.selected").last().data('value'), 10);
  $(this).parents(".rating-widget").first().find('.rating-form input[name=rating]').val($ratingValue);
  $(this).parents(".rating-widget").first().find('.rating').html("<span> Your rating for this movie is: " +$ratingValue+ " stars.</span>");
  }); 
});
