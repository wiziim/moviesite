<?php

require_once('functions.inc');
if (!isset($_POST['submit'])) {die(header("Location: register.php"));}

$_SESSION['formAttempt'] = true;

if (isset($_SESSION['error'])) {unset($_SESSION['error']);}

$_SESSION['error'] = array();

$required = array("uname","password1","password2");
foreach ($required as $requiredField)
{
	if(!isset($_POST[$requiredField]) || $_POST[$requiredField]== "")
	{ $_SESSION['error'][] = $requiredField . " is required."; }
}

if (!preg_match('/^[\w .]+$/',$_POST['uname']))
{
	$_SESSION['error'][] = "Username must be letters and numbers only.";
}

if ($_POST['password1'] != $_POST['password2']) {$_SESSION['error'][] = "Passwords don't match";}

if (count($_SESSION['error']) > 0) 
{
	die(header("Location: register.php"));
}
else 
{
	if(registerUser($_POST))
	{
		unset($_SESSION['formAttempt']);
		die(header("Location: login.php"));
	} 
	else 
	{
		error_log("Problem registering user: {$_POST['email']}");
		$_SESSION['error'][] = "Problem registering account";
		die(header("Location: register.php"));
	}
}

function registerUser($userData)
{
	$mysqli= new mysqli(DBHOST,DBUSER,DBPASS,DB);
	if($mysqli->connect_errno)
		{error_log("Can't connect to MYSQL: ". $msqli->connect_error);
			return false;}
	$uname=$mysqli->real_escape_string($_POST['uname']);
	$findUser="SELECT * from Users where username= '{$uname}'";
	$findResult=$mysqli->query($findUser);
	$findRow=$findResult->fetch_assoc();
	if($findRow!="")
	{
		$_SESSION['error'][]="A user with that username already exists";
		return false;
	}		
	$cryptedPassword=crypt($_POST['password1']);
	$password=$mysqli->real_escape_string($cryptedPassword);
	$query="INSERT into Users (username,password)"
				. "VALUES ('{$uname}','{$password}')";
	if($mysqli->query($query))
	{
		return true;
	}
	else
	{
		error_log("Problem inserting query {$query}");
		return false;
	}			

}
?>