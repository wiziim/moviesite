<?php

require_once("functions.inc");

if(!isset($_POST['submit'])){ die(header("Location: advform.php"));}

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["movieimg"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["movieimg"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["movieimg"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["movieimg"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


if (addMovie($_POST)){die(header("Location:home.php"));}


function addMovie($userData)
{
	$mysqli=new mysqli(DBHOST,DBUSER,DBPASS,DB);
	if ($mysqli->connect_errno) 
		{
			error_log("Can't connect to MYSQL" .$mysqli->connect_errno);
			return false;
		}

	$image=$_FILES['movieimg']['name'];
	$title=$_POST['title'];
	$genre=$_POST['genre'];
	$description=$_POST['description'];

	$query="INSERT into Movies(title,genre,description,image)"."VALUES('{$title}','{$genre}','{$description}','{$image}')";
	if($mysqli->query($query))
	{
		return true;
	}
}
?>
